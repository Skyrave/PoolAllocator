//
//  main.cpp
//  PoolAllocator
//
//  Created by Till Busse on 28/04/16.
//  Copyright © 2016 Till Busse. All rights reserved.
//

#include <iostream>
#include "PoolAllocator.h"

int main() {
    // insert code here...
    //std::cout << "Hello, World!\n";
    
    PoolAllocator<double, 10> double_pool;
    double * allocatedDouble = static_cast<double *>(double_pool.Allocate());
    *allocatedDouble = 50.5;
    std::cout << *allocatedDouble << " " << double_pool.Available() << std::endl;
    double_pool.Deallocate(allocatedDouble);
    std::cout << double_pool.Available() << std::endl;
    
    PoolAllocator<int, 10> int_pool;
    int * allocatedInt = static_cast<int *>(int_pool.Allocate());
    *allocatedInt = 50;
    std::cout << *allocatedInt << " " << int_pool.Available() << std::endl;
    int_pool.Deallocate(allocatedInt);
    std::cout << int_pool.Available() << std::endl;
    
    PoolAllocator<int, 1> int_pool_size1;
    int * allocatedInt1 = static_cast<int *>(int_pool_size1.Allocate());
    *allocatedInt1 = 50;
    std::cout << *allocatedInt1 << " " << int_pool_size1.Available() << std::endl;
    int_pool_size1.Deallocate(allocatedInt1);
    std::cout << int_pool_size1.Available() << std::endl;
    
    return 0;
}
