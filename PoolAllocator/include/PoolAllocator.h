//
//  PoolAllocator.h
//  PoolAllocator
//
//  Created by Till Busse on 28/04/16.
//  Copyright © 2016 Till Busse. All rights reserved.
//

#ifndef PoolAllocator_h
#define PoolAllocator_h

#include <iostream>

template<typename poolElementType, size_t elementNumber>
class IHeap
{
public:
    virtual void* Allocate(size_t size = sizeof(poolElementType)) = 0;
    virtual void Deallocate(void *) = 0;
    virtual size_t Available() const = 0;
};

template<typename poolElementType = u_long, size_t elementNumber = 1>
class PoolAllocator : public IHeap<poolElementType, elementNumber>
{
public:
    PoolAllocator() : mPoolAllocated{0} {};
    ~PoolAllocator(){};
    void* Allocate(size_t size = sizeof(poolElementType));
    void Deallocate(void *);
    size_t Available() const;
protected:
    poolElementType mPool[elementNumber];
    bool mPoolAllocated[elementNumber];
};


template<typename poolElementType, size_t elementNumber>
void*  PoolAllocator<poolElementType, elementNumber>::Allocate(size_t size)
{
    if (size>0 && size <= sizeof(poolElementType)) {
        if (Available()) {
            for (size_t i = 0; i < elementNumber; i++) {
                if (!mPoolAllocated[i]) {
                    mPoolAllocated[&mPoolAllocated[i]-mPoolAllocated] = true;
                    return &mPool[i];
                }
            }
        }
        return nullptr;
    }
    return nullptr;
}
template<typename poolElementType, size_t elementNumber>
size_t PoolAllocator<poolElementType, elementNumber>::Available() const
{
    size_t free = 0;
    for (size_t i = 0; i < elementNumber; i++) {
        if (!mPoolAllocated[i]) {
            free++;
        }
    }
    return free;
}

template<typename poolElementType, size_t elementNumber>
void PoolAllocator<poolElementType, elementNumber>::Deallocate(void *toDelete)
{
    mPoolAllocated[static_cast<poolElementType *>(toDelete) - mPool] = false;
}

#endif /* PoolAllocator_h */
